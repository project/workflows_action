<?php

namespace Drupal\workflows_action\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * The enable/disable form for content moderation notification entities.
 */
class WorkflowActionDisableForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    if ($this->entity->status()) {
      return $this->t('Disable workflow action %label?', ['%label' => $this->entity->label()]);
    }

    return $this->t('Enable workflow action %label?', ['%label' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    if ($this->entity->status()) {
      return $this->t('Emails will not be sent for this notification when it is disabled.');
    }

    return $this->t('Emails will be sent for this notification when it is enabled.');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->enttiy->toUrl('collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'workflow_action_disable_confirm_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->entity;
    if ($entity->status()) {
      $entity->disable();
    }
    else {
      $entity->enable();
    }
    $entity->save();

    $form_state->setRedirect('entity.workflow_action.collection');
  }

}

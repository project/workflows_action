<?php

namespace Drupal\workflows_action\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\workflows_field\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


/**
 * Class TransitionEventSubscriber.
 */
class TransitionEventSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * IdentificationEventSubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      WorkflowsFieldEvents::BEFORE_TRANSITION => ['preTransition'],
      WorkflowsFieldEvents::AFTER_TRANSITION => ['postTransition'],
    ];

    return $events;
  }

  /**
   * This method is called whenever the pre_transition event is dispatched.
   *
   * @param \Symfony\Component\EventDispatcher\Event $event
   *   The transition event.
   */
  public function preTransition(Event $event) {
    $this->handleTransition($event, 'pre');
  }

  /**
   * This method is called whenever the post_transition event is dispatched.
   *
   * @param \Symfony\Component\EventDispatcher\Event $event
   *   The transition event.
   */
  public function postTransition(Event $event) {
    $this->handleTransition($event, 'post');
  }

  /**
   * Yo ...
   *
   * @param \Symfony\Component\EventDispatcher\Event $event
   *   A transition event or any other.
   * @param string $event_type
   *   An event type, either 'pre' or 'post'.
   */
  protected function handleTransition(Event $event, string $event_type) {
    $workflow = $event->getWorkflow();
    $workflow_actions = $this->entityTypeManager
      ->getStorage('workflow_action')
      ->loadByProperties([
        'event' => $event_type,
        'workflow' => $workflow->id(),
      ]);
    $action_storage = $this->entityTypeManager->getStorage('action');
    $entities = [];
    // If it's a transition event, it can carry an entity.
    if ($event instanceof TransitionEventInterface) {
      $entities[] = $event->getEntity();
    }
    $transition_id = $event->getTransition()->id();
    foreach ($workflow_actions as $workflow_action) {
      if (!in_array($transition_id, $workflow_action->get('transitions'))) {
        continue;
      }
      /** @var \Drupal\Core\Action\ActionInterface[] $actions */
      $actions = $action_storage->loadMultiple($workflow_action->get('action'));
      $this->executeActions($actions, $entities);
    }
  }

  /**
   * Gogogo.
   *
   * @param \Drupal\system\Entity\Action[] $actions
   *   Action entities.
   * @param \Drupal\Core\Entity\EntityInterface[] $entities
   *   Entities to set.
   */
  protected function executeActions(array $actions, array $entities) {
    foreach ($actions as $action) {
      $action->execute($entities);
    }
  }

}

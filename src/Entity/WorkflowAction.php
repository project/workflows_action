<?php

namespace Drupal\workflows_action\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Workflows action entity.
 *
 * @ConfigEntityType(
 *   id = "workflow_action",
 *   label = @Translation("Workflow action"),
 *   handlers = {
 *     "list_builder" = "Drupal\workflows_action\WorkflowActionListBuilder",
 *     "form" = {
 *       "add" = "Drupal\workflows_action\Form\WorkflowActionForm",
 *       "edit" = "Drupal\workflows_action\Form\WorkflowActionForm",
 *       "delete" = "Drupal\workflows_action\Form\WorkflowActionDeleteForm",
 *       "disable" = "Drupal\workflows_action\Form\WorkflowActionDisableForm",
 *       "enable" = "Drupal\workflows_action\Form\WorkflowActionDisableForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   links = {
 *     "add-form" = "/admin/config/workflow/actions/add",
 *     "edit-form" = "/admin/config/workflow/actions/manage/{workflow_action}",
 *     "delete-form" = "/admin/config/workflow/actions/manage/{workflow_action}/delete",
 *     "enable-form" = "/admin/config/workflow/actions/manage/{workflow_action}/enable",
 *     "disable-form" = "/admin/config/workflow/actions/manage/{workflow_action}/disable",
 *     "collection" = "/admin/config/workflow/notifications"
 *   },
 *   config_prefix = "workflow_action",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "workflow",
 *     "transitions",
 *     "event",
 *     "action",
 *     "label",
 *   },
 * )
 */
class WorkflowAction extends ConfigEntityBase implements WorkflowActionInterface {

  /**
   * The Workflows action ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Workflows action label.
   *
   * @var string
   */
  protected $label;

  /**
   * Event type: pre or post transition.
   *
   * @var string
   */
  protected $event;

  /**
   * Action ID.
   *
   * @var string
   */
  protected $action;

  /**
   * The transition IDs relevant to this notification.
   *
   * @var string[]
   */
  public $transitions = [];

  /**
   * The associated workflow for these notifications.
   *
   * @var string
   */
  public $workflow;

}

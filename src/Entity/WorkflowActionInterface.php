<?php

namespace Drupal\workflows_action\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining workflow_action entities.
 */
interface WorkflowActionInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
